﻿using Game.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Game
{
    public partial class GameForm : Form
    {
        private readonly Dictionary<CellStates, Bitmap> bitmaps = new Dictionary<CellStates, Bitmap>();
        private readonly GameState gameState;
        private int tickCount;
        private bool isEnd = false;
        private Random rnd = new Random();

        public GameForm(DirectoryInfo imagesDirectory = null)
        {
            Game.Run();
            gameState = new GameState();
            FormBorderStyle = FormBorderStyle.FixedDialog;

            if (imagesDirectory == null)
                imagesDirectory = new DirectoryInfo(@"../../Images");
            foreach (var e in imagesDirectory.GetFiles("*.png"))
            {
                if (e.Name.StartsWith("N_") || e.Name.StartsWith("Map"))
                    continue;
                var name = CellStates.Empty;
                if (Enum.TryParse(e.Name.Substring(0, e.Name.Length - 4), true, out name))
                    bitmaps[name] = (Bitmap)Image.FromFile(e.FullName);
                else
                    throw new NotImplementedException("Error while texture download");
            }
            var timer = new Timer();
            timer.Interval = 250;
            timer.Tick += OnUpdate;
            timer.Start();
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Text = "Game";
            DoubleBuffered = true;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            Game.KeyPressed = e.KeyCode;
            if (e.KeyCode == Keys.W)
                bitmaps[CellStates.Player] = bitmaps[CellStates.PlayerW];
            if (e.KeyCode == Keys.S)
                bitmaps[CellStates.Player] = bitmaps[CellStates.PlayerS];
            if (e.KeyCode == Keys.A)
                bitmaps[CellStates.Player] = bitmaps[CellStates.PlayerA];
            if (e.KeyCode == Keys.D)
                bitmaps[CellStates.Player] = bitmaps[CellStates.PlayerD];
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            const int elSz = GameState.ElementSize;
            e.Graphics.FillRectangle(
                Brushes.Black, 0, 0, GameState.ElementSize * Map.MapWidth,
                GameState.ElementSize * Map.MapHeight);

            for (int x = 0; x < Map.MapWidth; x++)
            {
                for (int y = 0; y < Map.MapHeight; y++)
                {
                    e.Graphics.DrawImage(bitmaps[CellStates.Empty], x * elSz, y * elSz, elSz, elSz);
                    if (!Wall.NotWall(x, y))
                        e.Graphics.DrawImage(bitmaps[CellStates.Wall], x * elSz, y * elSz, elSz, elSz);
                    else if (GameDoor.NotOpenedDoor(x, y))
                        e.Graphics.DrawImage(bitmaps[GameDoor.DoorType(x, y)], x * elSz, y * elSz, elSz, elSz);
                    else if (GameKey.NonCollectedKey(x, y))
                        e.Graphics.DrawImage(bitmaps[GameKey.KeyType(x, y)], x * elSz, y * elSz, elSz, elSz);
                    e.Graphics.DrawImage(bitmaps[Map.field[x, y]], x * elSz, y * elSz, elSz, elSz);
                }
            }

            //e.Graphics.DrawImage(Image.FromFile(@"../../Images/N_Dark.png"), -1325 + Game.Player.X * GameState.ElementSize, -975 + Game.Player.Y * GameState.ElementSize);
            e.Graphics.ResetTransform();
        }

        private void OnUpdate(object sender, EventArgs args)
        {
            gameState.StartUpdate();
            if (tickCount == 2)
            {
                gameState.StartEnemyUpdate();
                tickCount = 0;
            }
            if (rnd.Next(0, 5) == 2)
                gameState.StartEnemyUpdate();
            tickCount++;
            gameState.EndUpdate();

            Game.KeyPressed = Keys.None;

            if (GameEnded())
            {
                GoToEndScreen();
            }

            Invalidate();
        }

        private void GoToEndScreen()
        {
            isEnd = true;
            Hide();
            var endForm = new EndForm();
            endForm.Show();
        }

        private bool GameEnded()
        {
            return (Game.IsGameOver && !isEnd) || (Game.IsWin && !isEnd);
        }
    }
}