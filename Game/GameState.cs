﻿using Game.Models;

namespace Game
{
    public class GameState
    {
        public const int ElementSize = 50;

        internal void EndUpdate()
        {
            ClearField();
            CheckConflicts();
        }

        internal void StartUpdate()
        {
            for (var x = 0; x < Map.MapWidth; x++)
                for (var y = 0; y < Map.MapHeight; y++)
                {
                    var cellState = Map.field[x, y];
                    if (cellState == CellStates.Player)
                    {
                        Game.Player.Act(x, y);
                    }
                }
        }

        private static void CheckConflicts()
        {
            foreach (var enemy in Game.Enemies)
            {
                Map.field[enemy.X, enemy.Y] = CellStates.Enemy;
                if (enemy.X == Game.Player.X && enemy.Y == Game.Player.Y)
                {
                    Game.Player.OnConflict(enemy);
                }
            }
            foreach (var key in Game.GameKeys)
            {
                if (key.X == Game.Player.X && key.Y == Game.Player.Y)
                {
                    Game.Player.OnConflict(key);
                }
            }

            foreach (var door in Game.GameDoors)
            {
                if (door.X == Game.Player.X && door.Y == Game.Player.Y)
                {
                    Game.Player.OnConflict(door);
                }
            }
            if (Game.Player.X == Game.Exit.X && Game.Player.Y == Game.Exit.Y)
            {
                Game.Player.OnConflict(Game.Exit);
            }
            Map.field[Game.Enter.X, Game.Enter.Y] = CellStates.Enter;
            Map.field[Game.Exit.X, Game.Exit.Y] = CellStates.Exit;
            Map.field[Game.Player.X, Game.Player.Y] = CellStates.Player;
        }

        private static void ClearField()
        {
            for (int x = 0; x < Map.MapWidth; x++)
            {
                for (int y = 0; y < Map.MapHeight; y++)
                {
                    if (Map.field[x, y] == CellStates.Player || Map.field[x, y] == CellStates.Enemy)
                    {
                        if (Wall.NotWall(x, y))
                            Map.field[x, y] = CellStates.Empty;
                        else
                            Map.field[x, y] = CellStates.Wall;
                    }
                    if (Game.Enter.X == x && Game.Enter.Y == y)
                    {
                        Map.field[x, y] = CellStates.Enter;
                    }
                }
            }
        }

        internal void StartEnemyUpdate()
        {
            for (var x = 0; x < Map.MapWidth; x++)
                for (var y = 0; y < Map.MapHeight; y++)
                {
                    var cellState = Map.field[x, y];
                    if (cellState == CellStates.Enemy)
                    {
                        for (int i = 0; i < Game.enemiesCount; i++)
                        {
                            if (Game.Enemies[i].X == x && Game.Enemies[i].Y == y)
                                Game.Enemies[i].Act(x, y);
                        }
                    }
                }
        }
    }
}