﻿using System.Drawing;
using System.Windows.Forms;

namespace Game
{
    public partial class EndForm : Form
    {
        public EndForm()
        {
            InitializeComponent();
            var picture = new PictureBox
            {
                Image = new Bitmap(Game.IsGameOver ? @"../../Images/YouDied.jpg" : @"../../Images/YouWin.jpg"),
                Location = new Point(0, 0),
                AutoSize = true,
            };
            Controls.Add(picture);
            var button = new Button()
            {
                Text = "Закрыть",
                Location = new Point(ClientSize.Width / 2, picture.Bottom)
            };
            Controls.Add(button);
            button.Click += (sender, e) =>
            {
                Application.Exit();
            };
        }
    }
}