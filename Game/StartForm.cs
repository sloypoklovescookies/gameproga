﻿using System.Drawing;
using System.Windows.Forms;

namespace Game
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
            var picture = new PictureBox
            {
                Image = new Bitmap(@"../../Images/Player.png"),
                Location = new Point(ClientSize.Width / 2, 0),
                AutoSize = true,
                Size = new Size(GameState.ElementSize, GameState.ElementSize)
            };
            Controls.Add(picture);
            var button = new Button()
            {
                Text = "Начать игру",
                Location = new Point(ClientSize.Width / 2, picture.Bottom)
            };
            Controls.Add(button);
            var gameForm = new GameForm();
            button.Click += (sender, e) =>
            {
                Hide();
                gameForm.Show();
            };
            gameForm.Closed += (sender, e) =>
            {
                this.Close();
            };
        }
    }
}