﻿using Game.Models;

namespace Game
{
    public class Exit : ICreature
    {
        public readonly int X = Map.MapWidth - 1;
        public readonly int Y = 0;

        public void Act(int x, int y)
        {
            throw new System.NotImplementedException();
        }

        public bool OnConflict(ICreature conflictedObject)
        {
            throw new System.NotImplementedException();
        }
    }
}