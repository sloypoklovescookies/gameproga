﻿using Game.Models;
using System;

namespace Game
{
    public class GameDoor : ICreature

    {
        public CellStates type;
        public int X;
        public int Y;

        public GameDoor(CellStates type, int x, int y)
        {
            this.type = type;
            X = x;
            Y = y;
        }

        public static bool IsDoor(int x, int y)
        {
            foreach (var door in Game.GameDoors)
            {
                if (door.X == x && door.Y == y)
                    return true;
            }
            return false;
        }

        internal static bool HaveKey(int x, int y)
        {
            var doorType = GameDoor.DoorType(x, y);
            foreach (var key in Game.CollectedKeys)
            {
                if (key.type == CellStates.SkeletonKey && doorType == CellStates.SkeletonDoor
                    || key.type == CellStates.DefaultKey && doorType == CellStates.DefaultDoor)
                    return true;
            }
            return false;
        }

        internal static CellStates DoorType(int x, int y)
        {
            foreach (var door in Game.GameDoors)
            {
                if (door.X == x && door.Y == y)
                    return door.type;
            }
            return CellStates.Empty;
        }

        internal static bool NotOpenedDoor(int x, int y)
        {
            foreach (var door in Game.GameDoors)
            {
                if (door.X == x && door.Y == y)
                    foreach (var colDoor in Game.OpenedDoors)
                    {
                        if (colDoor.X == x && colDoor.Y == y)
                        {
                            return false;
                        }
                    }
            }
            return true;
        }

        public void Act(int x, int y)
        {
            throw new NotImplementedException();
        }

        public bool OnConflict(ICreature conflictedObject)
        {
            throw new NotImplementedException();
        }
    }
}