﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Game.Models
{
    public static class Map
    {
        public static CellStates[,] field;
        public static readonly int MapWidth = 20;
        public static readonly int MapHeight = 15;
        public static readonly List<Wall> Walls;

        static Map()
        {
            field = new CellStates[MapWidth, MapHeight];
            Walls = new List<Wall>();
            for (int x = 0; x < field.GetLength(0); x++)
            {
                for (int y = 0; y < field.GetLength(1); y++)
                {
                    field[x, y] = CellStates.Empty;
                }
            }
            LoadMapFromImage();
        }

        private static void LoadMapFromImage(DirectoryInfo imagesDirectory = null)
        {
            if (imagesDirectory == null)
                imagesDirectory = new DirectoryInfo(@"../../Images");
            var map = (Bitmap)Image.FromFile(
                imagesDirectory.GetFiles()
                .Where(w => w.Name.StartsWith("Map"))
                .First()
                .FullName);
            for (int x = 0; x < MapWidth; x++)
            {
                for (int y = 0; y < MapHeight; y++)
                {
                    var pixel = map.GetPixel(x, y);
                    if (pixel.R == 255 && pixel.B == 0 && pixel.G == 0)
                    {
                        field[x, y] = CellStates.Player;
                    }
                    if (pixel.R == 0 && pixel.B == 0 && pixel.G == 0)
                    {
                        field[x, y] = CellStates.Wall;
                        Walls.Add(new Wall(x, y));
                    }
                    if (pixel.R == 0 && pixel.B == 255 && pixel.G == 255)
                    {
                        field[x, y] = CellStates.SkeletonKey;
                        Game.GameKeys.Add(new GameKey(CellStates.SkeletonKey, x, y));
                    }
                    if (pixel.R == 0 && pixel.B == 255 && pixel.G == 0)
                    {
                        field[x, y] = CellStates.SkeletonDoor;
                        Game.GameDoors.Add(new GameDoor(CellStates.SkeletonDoor, x, y));
                    }
                    if (pixel.R == 255 && pixel.B == 0 && pixel.G == 255)
                    {
                        field[x, y] = CellStates.DefaultKey;
                        Game.GameKeys.Add(new GameKey(CellStates.DefaultKey, x, y));
                    }
                    if (pixel.R == 0 && pixel.B == 0 && pixel.G == 255)
                    {
                        field[x, y] = CellStates.DefaultDoor;
                        Game.GameDoors.Add(new GameDoor(CellStates.DefaultDoor, x, y));
                    }
                }
            }
        }
    }

    public enum CellStates
    {
        Empty,
        Wall,
        Enemy,
        Enter,
        Exit,
        Player,
        SkeletonKey,
        SkeletonDoor,
        DefaultKey,
        DefaultDoor,
        PlayerW,
        PlayerA,
        PlayerS,
        PlayerD,
        Error
    }
}