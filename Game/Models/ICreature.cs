﻿namespace Game
{
    public interface ICreature
    {
        void Act(int x, int y);

        bool OnConflict(ICreature conflictedObject);
    }
}