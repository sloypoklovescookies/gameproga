﻿namespace Game.Models
{
    public class Wall
    {
        public int X;
        public int Y;

        public Wall(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static bool NotWall(int x, int y)
        {
            foreach (var wall in Map.Walls)
            {
                if (wall.X == x && wall.Y == y)
                    return false;
            }
            return true;
        }
    }
}