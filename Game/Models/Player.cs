﻿using Game.Models;
using System.Windows.Forms;

namespace Game
{
    public class Player : ICreature
    {
        public int X;
        public int Y;

        public Player()
        {
            X = 0;
            Y = Map.MapHeight - 2;
        }

        public void Act(int x, int y)
        {
            if (y > 0 && Wall.NotWall(X, Y - 1))
            {
                if (GameDoor.IsDoor(X, Y - 1) && !GameDoor.HaveKey(X, Y - 1))
                    return;
                if (Game.KeyPressed == Keys.W)
                    this.Y--;
            }
            if (y < Map.MapHeight - 1 && Wall.NotWall(X, Y + 1))
            {
                if (GameDoor.IsDoor(X, Y + 1) && !GameDoor.HaveKey(X, Y + 1))
                    return;
                if (Game.KeyPressed == Keys.S)
                    this.Y -= -1;
            }
            if (x > 0 && Wall.NotWall(X - 1, Y))
            {
                if (GameDoor.IsDoor(X - 1, Y) && !GameDoor.HaveKey(X - 1, Y))
                    return;
                if (Game.KeyPressed == Keys.A)
                    this.X--;
            }
            if (x < Map.MapWidth - 1 && Wall.NotWall(X + 1, Y))
            {
                if (GameDoor.IsDoor(X + 1, Y) && !GameDoor.HaveKey(X + 1, Y))
                    return;
                if (Game.KeyPressed == Keys.D)
                    this.X -= -1;
            }
        }

        public bool OnConflict(ICreature conflictedObject)
        {
            if (conflictedObject is Enemy)
            {
                Game.IsGameOver = true;
                return true;
            }
            if (conflictedObject is Exit)
            {
                Game.IsWin = true;
                return false;
            }
            if (conflictedObject is GameKey)
            {
                Game.CollectKey(conflictedObject);
                return false;
            }
            if (conflictedObject is GameDoor)
            {
                Game.OpenDoor(conflictedObject);
                return false;
            }
            return false;
        }
    }
}