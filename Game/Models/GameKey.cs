﻿using Game.Models;

namespace Game
{
    public class GameKey : ICreature
    {
        public CellStates type;
        public int X;
        public int Y;

        public GameKey(CellStates type, int x, int y)
        {
            this.type = type;
            X = x;
            Y = y;
        }

        public void Act(int x, int y)
        {
            throw new System.NotImplementedException();
        }

        public bool OnConflict(ICreature conflictedObject)
        {
            throw new System.NotImplementedException();
        }

        internal static CellStates KeyType(int x, int y)
        {
            foreach (var key in Game.GameKeys)
            {
                if (key.X == x && key.Y == y)
                    return key.type;
            }
            return CellStates.Empty;
        }

        public static bool NonCollectedKey(int x, int y)
        {
            foreach (var key in Game.GameKeys)
            {
                if (key.X == x && key.Y == y)
                    foreach (var collKey in Game.CollectedKeys)
                    {
                        if (collKey.X == x && collKey.Y == y)
                        {
                            return false;
                        }
                    }
            }
            return true;
        }
    }
}