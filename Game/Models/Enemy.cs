﻿using Game.Models;
using System;

namespace Game
{
    public class Enemy : ICreature
    {
        public int X;
        public int Y;
        private Random rnd = new Random();

        public Enemy(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void Act(int x, int y)
        {
            if (Math.Sqrt(Math.Pow(x - Game.Player.X, 2) + Math.Pow(y - Game.Player.Y, 2)) > 5)
                return;
            var dx = 0;
            var dy = 0;
            if (rnd.Next(0, 2) == 0)
            {
                dx = MoveX(x);
            }
            else
            {
                dy = MoveY(y);
            }
            foreach (var enemy in Game.Enemies)
            {
                if (enemy.X == X + dx && enemy.Y == Y + dy)
                    return;
            }
            X += dx;
            Y += dy;
        }

        public bool OnConflict(ICreature conflictedObject)
        {
            throw new System.NotImplementedException();
        }

        private int MoveX(int x)
        {
            if (x > 0)
                if (Game.Player.X < X)
                {
                    return -1;
                }
            if (x < Map.MapWidth - 1)
                if (Game.Player.X > X)
                {
                    return 1;
                }
            return 0;
        }

        private int MoveY(int y)
        {
            if (y > 0)
                if (Game.Player.Y < Y)
                {
                    return -1;
                }
            if (y < Map.MapHeight - 1)
                if (Game.Player.Y > Y)
                {
                    return 1;
                }
            return 0;
        }
    }
}