﻿using Game.Models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Game
{
    public static class Game
    {
        public static HashSet<GameKey> CollectedKeys;
        public static HashSet<GameDoor> OpenedDoors;
        public static HashSet<GameKey> GameKeys;
        public static HashSet<GameDoor> GameDoors;
        public static Player Player;
        public static Enter Enter;
        public static Exit Exit;
        public static List<Enemy> Enemies;
        public static readonly int enemiesCount = 3;
        private static Random rnd = new Random();
        internal static Keys KeyPressed = Keys.None;
        public static bool IsGameOver;
        public static bool IsWin;

        public static void Run()
        {
            GameKeys = new HashSet<GameKey>();
            CollectedKeys = new HashSet<GameKey>();
            GameDoors = new HashSet<GameDoor>();
            OpenedDoors = new HashSet<GameDoor>();
            IsGameOver = false;
            IsWin = false;
            Player = new Player();
            Enter = new Enter();
            Exit = new Exit();
            Enemies = new List<Enemy>();
            CollectedKeys = new HashSet<GameKey>();
            while (Enemies.Count < enemiesCount)
            {
                var newX = rnd.Next(1, Map.MapWidth - 2);
                var newY = rnd.Next(1, Map.MapHeight - 2);
                if (Math.Sqrt(Math.Pow(newX - Player.X, 2) + Math.Pow(newY - Player.Y, 2)) > 9)
                    Enemies.Add(new Enemy(newX, newY));
            }

            foreach (var enemy in Enemies)
            {
                Map.field[enemy.X, enemy.Y] = CellStates.Enemy;
            }
            Map.field[Enter.X, Enter.Y] = CellStates.Enter;
            Map.field[Exit.X, Exit.Y] = CellStates.Exit;
        }

        internal static void CollectKey(ICreature conflictedObject)
        {
            CollectedKeys.Add((GameKey)conflictedObject);
        }

        internal static void OpenDoor(ICreature conflictedObject)
        {
            OpenedDoors.Add((GameDoor)conflictedObject);
        }
    }
}