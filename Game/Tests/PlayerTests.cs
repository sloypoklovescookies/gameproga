﻿using Game.Models;
using NUnit.Framework;

namespace Game.Tests
{
    [TestFixture]
    internal class PlayerTests
    {
        [Test]
        public static void PlayerSmokeTest()
        {
            var player = new Player();
            Assert.AreEqual(player.X, 0);
            Assert.AreEqual(player.Y, Map.MapHeight - 2);
        }

        [Test]
        public static void PlayerIsActing()
        {
            var player = new Player();
            var x = player.X;
            var y = player.Y;
            player.Act(player.X, player.Y);
            Assert.IsFalse(x == player.X && y == player.Y);
        }

        [Test]
        public static void PlayerIsConglictingEnemy()
        {
            var player = new Player();
            Assert.IsTrue(player.OnConflict(new Enemy(1, 1)));
        }

        [Test]
        public static void PlayerIsConglictingExit()
        {
            var player = new Player();
            Assert.IsFalse(player.OnConflict(new Exit()));
        }
    }
}