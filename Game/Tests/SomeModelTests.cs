﻿using Game.Models;
using NUnit.Framework;

namespace Game.Tests
{
    [TestFixture]
    internal class SomeModelTests
    {
        [Test]
        public static void ExitSmoke()
        {
            var exit = new Exit();
            Assert.IsTrue(exit != null);
        }

        [Test]
        public static void ExitTest()
        {
            var exit = new Exit();
            Assert.IsTrue(exit.X == Map.MapWidth - 1 && exit.Y == 0);
        }

        [Test]
        public static void EnterSmoke()
        {
            var exit = new Enter();
            Assert.IsTrue(exit != null);
        }

        [Test]
        public static void EnterTest()
        {
            var enter = new Enter();
            Assert.IsTrue(enter.X == 0 && enter.Y == Map.MapHeight - 1);
        }
    }
}