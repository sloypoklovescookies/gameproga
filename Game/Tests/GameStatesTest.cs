﻿using NUnit.Framework;

namespace Game.Tests
{
    [TestFixture]
    internal class GameStatesTest
    {
        [Test]
        public static void SmokeTest()
        {
            Game.Run();
            Assert.IsFalse(Game.IsWin);
            Assert.IsFalse(Game.IsGameOver);
        }

        [Test]
        public static void PlayerMoved()
        {
            Game.Run();
            var gs = new GameState();
            Game.Player.X = 4;
            Game.Player.Y = 4;
            Game.Enemies[0].X = 4;
            Game.Enemies[0].Y = 4;
            gs.EndUpdate();
            Assert.IsTrue(Game.IsGameOver);
        }
    }
}