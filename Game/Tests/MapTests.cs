﻿using Game.Models;
using NUnit.Framework;

namespace Game.Tests
{
    internal class MapTests
    {
        [Test]
        public static void SmokeTest()
        {
            Game.Run();
            Assert.IsTrue(Map.field != null);
            Assert.IsTrue(Map.MapHeight > 0);
            Assert.IsTrue(Map.MapWidth > 0);
        }

        [Test]
        public static void EnemiesOnMap()
        {
            Game.Run();
            foreach (var enemy in Game.Enemies)
            {
                Assert.AreEqual(Map.field[enemy.X, enemy.Y], CellStates.Enemy);
            }
        }

        [Test]
        public static void PlayerMoved()
        {
            Game.Run();
            var gs = new GameState();
            var old = new Player();
            old.X = Game.Player.X;
            old.Y = Game.Player.Y;
            Game.KeyPressed = System.Windows.Forms.Keys.W;
            gs.StartUpdate();
            gs.EndUpdate();
            Assert.IsFalse(old.X == Game.Player.X && old.Y == Game.Player.Y);
        }
    }
}