﻿using Game.Models;
using NUnit.Framework;
using System;

namespace Game.Tests
{
    [TestFixture]
    internal class GameTests
    {
        [Test]
        public static void GameRuner()
        {
            Game.Run();
        }

        [Test]
        public static void SmokeTest()
        {
            Game.Run();
            Assert.IsTrue(Game.Player != null);
            Assert.IsTrue(Game.Exit != null);
            Assert.IsTrue(Game.Enter != null);
            Assert.IsTrue(Game.Enemies != null);
            Assert.IsTrue(Game.IsGameOver || Game.IsWin == false);
        }

        [Test]
        public static void EnemyCount()
        {
            Game.Run();
            Assert.AreEqual(Game.Enemies.Count, Game.enemiesCount);
        }

        [Test]
        public static void EnemiesFarEnough()
        {
            Game.Run();
            foreach (var enemy in Game.Enemies)
            {
                Assert.IsTrue(Math.Sqrt(Math.Pow(enemy.X - Game.Player.X, 2) + Math.Pow(enemy.Y - Game.Player.Y, 2)) > 7);
            }
        }

        [Test]
        public static void EnemiesAreEnemies()
        {
            Game.Run();
            foreach (var enemy in Game.Enemies)
            {
                Assert.AreEqual(Map.field[enemy.X, enemy.Y], CellStates.Enemy);
            }
        }
    }
}